package com.dmc.bitcoin;

import android.util.Log;

public class Constants {
    private static final String BASEURL = "http://jsonviewer.stack.hu/";

    public static String getURL(String path){
        Log.e("path",BASEURL+path);
        return BASEURL+path;
    }

}
