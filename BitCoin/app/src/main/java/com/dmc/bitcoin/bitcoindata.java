package com.dmc.bitcoin;

import java.io.Serializable;

public class bitcoindata implements Serializable {


        private int code;
        private String currency_symbol;
        private int rate;
        private  String discription;

    public bitcoindata() {
    }

    public bitcoindata(int code, String currency_symbol, int rate, String discription) {
        this.code = code;
        this.currency_symbol = currency_symbol;
        this.rate = rate;
        this.discription = discription;
    }

    public int getCode() {
        return code;
    }

    public String getCurrency_symbol() {
        return currency_symbol;
    }

    public int getRate() {
        return rate;
    }

    public String getDiscription() {
        return discription;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setCurrency_symbol(String currency_symbol) {
        this.currency_symbol = currency_symbol;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    @Override
    public String toString() {
        return "bitcoindata{" +
                "code=" + code +
                ", currency_symbol='" + currency_symbol + '\'' +
                ", rate=" + rate +
                ", discription='" + discription + '\'' +
                '}';
    }
}
